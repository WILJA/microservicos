package cloud.disciplina;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "core-service", fallback = CoreClientFallback.class)
public interface CoreClient {

	@RequestMapping(value = "/carrinhos", method = RequestMethod.GET)
    Resources<CoreDTO> getAllItens();

    @RequestMapping(value = "/carrinhos/{id}/dto", method = RequestMethod.GET)
    CoreDTO getItem(@PathVariable("id") Long id);
	
}