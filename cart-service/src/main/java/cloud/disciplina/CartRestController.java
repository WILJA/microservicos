package cloud.disciplina;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/carts")
public class CartRestController {

	
	@Autowired
	CoreClient coreClient;

	@GetMapping("/{id}/dto")
	@SuppressWarnings("all")
	public CartDTO getCart(@PathVariable Long id) throws Exception {
		Resources<CoreDTO> coreDto = coreClient.getAllItens();
		List<String> coreDtos = coreDto.getContent().stream()
				.map(a -> a.getNome()).collect(Collectors.toList());
		
		return CartDTO.builder()
				.itensCarrinho(coreDtos)
				.build();

	}		
	
}