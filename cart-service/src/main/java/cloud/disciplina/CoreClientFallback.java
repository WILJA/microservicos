package cloud.disciplina;

import java.util.ArrayList;

import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Component;

@Component
public class CoreClientFallback implements CoreClient {

	@Override
	public CoreDTO getItem(Long id) {
		return new CoreDTO();
	}
	
	@Override
	public Resources<CoreDTO> getAllItens() {				
		return new Resources<>(new ArrayList<>());
	}

}