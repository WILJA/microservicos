package core.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cloud.core.DisciplinaServiceProxy;
import cloud.dto.UsuarioDTO;
import cloud.model.Usuario;
import cloud.repository.UsuarioRepository;
import cloud.service.UsuarioService;
import cloud.support.AbstractController;



@RestController
@RequestMapping("/Usuarios")
public class UsuarioRestController  extends AbstractController<Usuario, UsuarioService> {
	
    protected static Logger log = LoggerFactory.getLogger(UsuarioRestController.class);
	
	@Autowired
	UsuarioRepository repository;
	
	@Autowired
	DisciplinaServiceProxy disciplinaProxy;
	
	@PreAuthorize("#oauth2.isUser()")
	@GetMapping("/nomes")
	public List<String> getUsuarios() {
		return repository.findAll()
				.stream().map(a -> a.getLogin()).collect(Collectors.toList());
	}

	@GetMapping("/{id}/dto")
	@SuppressWarnings("all")
	public UsuarioDTO getUsuario(@PathVariable Long id) throws Exception {

		//List<String> nomesDisciplinas = disciplinaProxy.getNomesDisciplinas();
				
		Usuario Usuario = repository.findOne(id);

		return UsuarioDTO.builder().idUsuario(Usuario.getIdUsuario())
				.idUsuario(Usuario.getIdUsuario())
				.login(Usuario.getLogin())
				.senha(Usuario.getSenha())
				.idCliente(Usuario.getIdCliente())
				.grupo(Usuario.getGrupo())
				.build();

	}

	
}