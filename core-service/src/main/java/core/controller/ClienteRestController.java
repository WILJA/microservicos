package core.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cloud.core.DisciplinaServiceProxy;
import cloud.dto.ClienteDTO;
import cloud.model.Cliente;
import cloud.repository.ClienteRepository;
import cloud.service.ClienteService;
import cloud.support.AbstractController;



@RestController
@RequestMapping("/clientes")
public class ClienteRestController  extends AbstractController<Cliente, ClienteService> {
	
    protected static Logger log = LoggerFactory.getLogger(ClienteRestController.class);
	
	@Autowired
	ClienteRepository repository;
	
	@Autowired
	DisciplinaServiceProxy disciplinaProxy;
	
	@PreAuthorize("#oauth2.isUser()")
	@GetMapping("/nomes")
	public List<String> getClientes() {
		return repository.findAll()
				.stream().map(a -> a.getNome()).collect(Collectors.toList());
	}

	@GetMapping("/{id}/dto")
	@SuppressWarnings("all")
	public ClienteDTO getCliente(@PathVariable Long id) throws Exception {

		//List<String> nomesDisciplinas = disciplinaProxy.getNomesDisciplinas();
				
		Cliente Cliente = repository.findOne(id);

		return ClienteDTO.builder().idCliente(Cliente.getIdCliente())
				.idCliente(Cliente.getIdCliente())
				.cpf(Cliente.getCpf())
				.nome(Cliente.getNome())
				.email(Cliente.getEmail())
				.idEndereco(Cliente.getIdEndereco())
				.build();

	}

	
}