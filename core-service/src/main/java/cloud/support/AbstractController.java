package cloud.support;

import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Abstract controller that encapsulates all boilerplate code needed to
 * createBatch a simple controller object
 * 
 * @param <E> entity object
 * @param <S> service that implements basic operations from {@link BaseService}
 */
public abstract class AbstractController<E extends BaseEntity, S extends BaseService<E>> implements BaseController<E> {

    protected static Logger log = LoggerFactory.getLogger(AbstractController.class);

    private static final SuccessResponse SUCCESS_RESPONSE = new SuccessResponse();

	@Autowired
	protected S service;

    public Page<E> all(@PageableDefault(page = 0, size = 10) Pageable pageable){
    	try
		{
    		 return service.list(pageable);
		}
		catch (ConstraintViolationException e)
		{
			e.printStackTrace();
		}
    	 return null;
       
    }

    public E create(@RequestBody E e){
        return service.save(e);
    }

    public E read(@PathVariable String id) {
    	try
		{

    		final Long entityId = Long.valueOf(id);
            final E entity = service.get(entityId);
            return entity;
		}
		catch (ConstraintViolationException e)
		{
			e.printStackTrace();
		} 
    	 return null;
       
    }

    public E update(@RequestBody E e, @PathVariable String id){
        final E obj = service.get(Long.valueOf(id));
        return obj == null ? null : service.save(e);
    }

    public Object delete(@PathVariable String id) {
        if (service.delete(Long.valueOf(id))) {
            return SUCCESS_RESPONSE;
        }
        final Long entityId = Long.valueOf(id);
        return new ErrorMessage(entityId, "no matches found");
    }
}