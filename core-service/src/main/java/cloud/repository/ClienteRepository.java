package cloud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import cloud.model.Cliente;

@RepositoryRestResource(path = "/clientes", collectionResourceRel = "clientes")
public interface ClienteRepository extends JpaRepository<Cliente, Long>{
	
}